package com.airport.model;

public class FlightRoute {
    private String sourceIata;
    private String sourceIcao;
    private String destIata;
    private String destIcao;
    private double distance;

    public String getSourceIata() {
        return sourceIata;
    }

    public void setSourceIata(String sourceIata) {
        this.sourceIata = sourceIata;
    }

    public String getSourceIcao() {
        return sourceIcao;
    }

    public void setSourceIcao(String sourceIcao) {
        this.sourceIcao = sourceIcao;
    }

    public String getDestIata() {
        return destIata;
    }

    public void setDestIata(String destIata) {
        this.destIata = destIata;
    }

    public String getDestIcao() {
        return destIcao;
    }

    public void setDestIcao(String destIcao) {
        this.destIcao = destIcao;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
