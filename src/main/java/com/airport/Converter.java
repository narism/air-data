package com.airport;

import com.airport.service.ConverterService;

import java.io.IOException;

public class Converter {


    public static void main(String[] args) throws IOException {
        ConverterService converterService = new ConverterService();
        converterService.generateRouteScripts();
    }
}
