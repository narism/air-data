package com.airport.service;

import com.airport.model.AirlineRoute;
import com.airport.model.Airport;
import com.airport.model.FlightRoute;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ConverterService {

  public void generateRouteScripts() throws IOException {
    CsvService csvService = new CsvService();
    List<Airport> airports = csvService.readAirportsFromCsv();
    List<AirlineRoute> airlineRoutes = csvService.readRoutesFromCsv();
    List<FlightRoute> resultRoutes = new ArrayList<>();
    airlineRoutes.forEach(airlineRoute -> {
      Optional<Airport> airportOp = airports.stream().filter(airport -> airport.getIata().equals(airlineRoute.getSourceIata())).findFirst();
      if (airportOp.isPresent()) {
        Airport sourceAirport = airportOp.get();
        Optional<Airport> destAirportOp = airports.stream().filter(airport -> airport.getIata().equals(airlineRoute.getDestIata())).findFirst();
        if (destAirportOp.isPresent()) {
          Airport destAirport = destAirportOp.get();
          double distMeters = calcDistance(sourceAirport.getLatitude(), destAirport.getLatitude(), sourceAirport.getLongitude(), destAirport.getLongitude(), sourceAirport.getAltitude(), destAirport.getAltitude());
          System.out.println(sourceAirport.getCity() + " " + destAirport.getCity() + " " + distMeters);
          FlightRoute flightRoute = new FlightRoute();
          flightRoute.setDestIata(destAirport.getIata());
          flightRoute.setDestIcao(destAirport.getIcao());
          flightRoute.setSourceIata(sourceAirport.getIata());
          flightRoute.setSourceIcao(sourceAirport.getIcao());
          flightRoute.setDistance(distMeters);
          resultRoutes.add(flightRoute);
        }
      }
    });
  }

  public double calcDistance(double sourceLatitude, double destLatitude, double sourceLongitude,
                             double destLongitude, double sourceAltitude, double destAltitude) {

    final int earthRadius = 6371; // Radius of the earth
    double latDistance = Math.toRadians(destLatitude - sourceLatitude);
    double lonDistance = Math.toRadians(destLongitude - sourceLongitude);
    double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
        + Math.cos(Math.toRadians(sourceLatitude)) * Math.cos(Math.toRadians(destLatitude))
        * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    double distance = earthRadius * c * 1000;

    double height = sourceAltitude - destAltitude;

    distance = Math.pow(distance, 2) + Math.pow(height, 2);

    return Math.sqrt(distance);
  }
}
