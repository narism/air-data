package com.airport.service;

import com.airport.model.Airport;
import com.airport.model.AirlineRoute;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class CsvService {

    public List<Airport> readAirportsFromCsv() throws IOException {
        InputStream inputStream = getClass().getResourceAsStream("/airport.dat");
        CsvMapper mapper = new CsvMapper();
        mapper.disable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
        CsvSchema schema = mapper.schemaFor(Airport.class);
        MappingIterator<Airport> it = mapper.readerFor(Airport.class).with(schema)
                .readValues(inputStream);
        return it.readAll();
    }

    public List<AirlineRoute> readRoutesFromCsv() throws IOException {
        InputStream inputStream = getClass().getResourceAsStream("/routes.dat");
        CsvMapper mapper = new CsvMapper();
        mapper.disable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
        CsvSchema schema = mapper.schemaFor(AirlineRoute.class);
        MappingIterator<AirlineRoute> it = mapper.readerFor(AirlineRoute.class).with(schema)
                .readValues(inputStream);
        return it.readAll();
    }
}
